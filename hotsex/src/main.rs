use std::net::TcpListener;

mod parser;
mod resource_loader;
mod hotsex_structs;
mod hotsex_transform;
mod casting;

fn main() {
    let listener = TcpListener::bind("127.0.0.1:8080").unwrap();

    for stream in listener.incoming() {
        match stream {
            Ok(stream) => {
                println!("new client!");
            }
            Err(e) => { /* connection failed */ }
        }
    }
}
