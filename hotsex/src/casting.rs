//! A common module for typecasting traits

// @FIXME: this should be changed or removed as soon as the official TryFrom
// trait is implemented, if it ever is
//! A trait that allows casting from a type T into Self with a possibility of
//! failure
pub trait TryFrom<T>: Sized {
    
    type Err;
    fn try_from(T) -> Result<Self, Self::Err>;

}
