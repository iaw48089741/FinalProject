//! A module containing all types that compose a HOTSEX program

use std::collections::HashMap;

#[derive(Clone)]
#[derive(PartialEq)]
pub enum HotsexAtom {
    Symbol(String),
    Text(String),
    Integer(i64),
    Float(f64)
}

/// An immutable representation of a HOTSEX node, which is a tree-like
/// structure similar to a DOM. Nodes may be either "passing nodes" with
/// children (branches) or atomic "end nodes" (leaves). Both have different
/// available fields
#[derive(Clone)]
pub enum HotsexNode {
    Leaf {
        value: HotsexAtom,
        tag: Option<String>
    },
    Branch {
        children: Vec<HotsexNode>,
        tag: Option<String>,
        meta: Option<Vec<HotsexNode>>,
        attributes: Option<Vec<HotsexNode>>
    }
}

/// An immutable representation of a HOTSEX file, with a list of all the
/// detected necessary dependencies
#[derive(Clone)]
pub struct HotsexTree {
    pub resource: String,
    pub dependencies: Option<Vec<String>>, // Rc?
    pub tree: HotsexNode // there may only be one root node
}

/// A small, intended to be short-lived representation of a HOTSEX file.
/// Contains references to all the necessary HotsexTrees required to form
/// a full file with this template.
#[derive(Clone)]
pub struct HotsexDocument<'a> {
    pub resources: HashMap<&'a str, &'a HotsexTree>,
    pub document: &'a str
}
