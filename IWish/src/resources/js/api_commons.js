"use strict";

var APIClientCore = {

    authItem: "IWishJwtApiAuthorization",
    log: function (fnName, message, errorLevel) {
        var logFunction;

        function generateLogger(type, useLogger) {
            return function (header, msg) {
                useLogger(type + " [" + header + "]: " + msg);
            }
        }
        
        var stdout = console.log.bind(console);
        var stderr = console.error.bind(console);

        switch (errorLevel) {
            case 1:
                logFunction = generateLogger("INFO", stdout);
                break;
            case 2:
                logFunction = generateLogger("SUCCESS", stdout);
                break;
            case 3:
                logFunction = generateLogger("WARN", stdout);
                break;
            case 4:
                logFunction = generateLogger("ERROR", stderr);
                break;
            default:
                logFunction = generateLogger("OTHER:", stdout);
        }

        logFunction("APIClientCore." + fnName, message);
    },
    decodeJwt: function (jwtString) {
        var splitToken = jwtString.split(".");
        try {
            return {
                header: JSON.parse(atob(splitToken[0])),
                payload: JSON.parse(atob(splitToken[1])),
                signature: splitToken[2]
            }
        } catch (err) {
            this.log("decodeJwt",
                     "Could not decode JWT - " + err.message,
                     4);
            return null;
        }
    },
    tokenIsValid: function (decodedToken) {
        // Check whether this token will be valid for more than 20
        // seconds from now on
        if (decodedToken == null || 
            decodedToken.payload == null ||
            decodedToken.payload.exp == null) {
            return false;
        }
        return decodedToken.payload.exp > (Date.now() / 1000 + 20);
    },
    call: function (params) {
        // REST command to actual URL
        params.url = "/api" + params.url;
        // Proxy pattern for success function
        params.success = (function (succ) {
            return function (data) {
                try {
                    succ(data);
                } catch (err) {
                    this.log("call " + params.url + " success", 
                             "Callback failed - " + err.message, 
                             4);
                }
            }
        })(params.success);
        // Set this object as the context
        params.context = this;
        // Before send to check for available valid JWT
        params.beforeSend = function (xhr) {
            var jwt = localStorage.getItem(this.authItem);
            if (jwt == null || !this.tokenIsValid(this.decodeJwt(jwt))) {
                // Token is invalid. We have to renew it before sending 
                // the request again
                $.ajax({
                    url: "/authorization/jwt",
                    context: this,
                    success: function (d)  {
                        if (d.error) {
                            this.log("call /authorization/jwt success",
                                     d.errorMessage,
                                     4);
                            return;
                        }
                        
                        localStorage.setItem(this.authItem, d.jwt);
                        
                        // attempt to resend the request
                        $.ajax(params);
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        this.log("call /authorization/jwt error", 
                                 textStatus + " - " + errorThrown,
                                 4);
                    }
                });
                return false;
            }
            
            xhr.setRequestHeader("Authorization", "Bearer " + jwt);
        }
        
        // Finally, send the request
        $.ajax(params);
    }

};
