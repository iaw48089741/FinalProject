$(document).ready(function() {

	$(window).scroll(function() {

		var winTop = $(window).scrollTop();

	    if(winTop >= 1) {
	    	$("body.profile").addClass("sticky-header");
            $(".image-profile").addClass("sticky-header-img");
            $(".settings").addClass("sticky-header-settings");
	    } else {
	    	$("body.profile").removeClass("sticky-header");
            $(".image-profile").removeClass("sticky-header-img");
            $(".settings").removeClass("sticky-header-settings");
	    }

	});

    $('.btn-search').click(function(){
        $('#CI_usuarios').toggle();
   /*     if($('#CI_usuarios').is(':visible')){
            $('#CI_usuarios').addClass('search-friend-input');
            $('#CI_usuarios').siblings('.btn-search').addClass('search-friend-btn');
        } else {
            $('#CI_usuarios').removeClass('search-friend-input');
            $('#CI_usuarios').siblings('.btn-search').removeClass('search-friend-btn');

        }*/
        
    });

	ajaxAutoComplete({inputId: "CI_usuarios", ajaxUrl: "/finder/"});

    //loadFriendList();

});

//**** Funciones globales ***///
function ajaxAutoComplete(options) {

    var defaults = {
        inputId:null,
        ajaxUrl:false,    
        data: {},
        minLength: 0
    };

    options = $.extend(defaults, options);
    var $input = $("#" + options.inputId);


    if (options.ajaxUrl){


        var $autocomplete = $('<ul id="ac" class="autocomplete-content dropdown-content"'
            + '></ul>'),
        $inputDiv = $input.closest('.input-field'),
        request,
        runningRequest = false,
        timeout,
        liSelected;

        if ($inputDiv.length) {
            $inputDiv.append($autocomplete); // Set ul in body
        } else {
            $input.after($autocomplete);
        }

        var highlight = function (string, match) {
        	console.log(string);
        	console.log(match);
            var matchStart = string.toLowerCase().indexOf("" + match.toLowerCase() + ""),
            matchEnd = matchStart + match.length - 1,
            beforeMatch = string.slice(0, matchStart),
            matchText = string.slice(matchStart, matchEnd + 1),
            afterMatch = string.slice(matchEnd + 1);
            string = "<span>" + beforeMatch + "<span class='highlight'>" + 
            matchText + "</span>" + afterMatch + "</span>";
            console.log(string);
            return string;

        };

        $autocomplete.on('click', 'li', function () {
            $input.val($(this).text().trim());
            $autocomplete.empty();
        });

        $input.on('keyup', function (e) {

            if (timeout) { // comment to remove timeout
                clearTimeout(timeout);
            }

            if (runningRequest) {
                request.abort();
            }

            if (e.which === 13) { // select element with enter key
                liSelected[0].click();
                return;
            }

            // scroll ul with arrow keys
            if (e.which === 40) {   // down arrow
                if (liSelected) {
                    liSelected.removeClass('selected');
                    next = liSelected.next();
                    if (next.length > 0) {
                        liSelected = next.addClass('selected');
                    } else {
                        liSelected = $autocomplete.find('li').eq(0).addClass('selected');
                    }
                } else {
                    liSelected = $autocomplete.find('li').eq(0).addClass('selected');
                }
                return; // stop new AJAX call
            } else if (e.which === 38) { // up arrow
                if (liSelected) {
                    liSelected.removeClass('selected');
                    next = liSelected.prev();
                    if (next.length > 0) {
                        liSelected = next.addClass('selected');
                    } else {
                        liSelected = $autocomplete.find('li').last().addClass('selected');
                    }
                } else {
                    liSelected = $autocomplete.find('li').last().addClass('selected');
                }
                return;
            } 

            // escape these keys
            if (e.which === 9 ||        // tab
                e.which === 16 ||       // shift
                e.which === 17 ||       // ctrl
                e.which === 18 ||       // alt
                e.which === 20 ||       // caps lock
                e.which === 35 ||       // end
                e.which === 36 ||       // home
                e.which === 37 ||       // left arrow
                e.which === 39) {       // right arrow
                return;
            } else if (e.which === 27) { // Esc. Close ul
                $autocomplete.empty();
                return;
            }

            var val = $input.val().toLowerCase();
            $autocomplete.empty();
            if (val.length > options.minLength) {

                timeout = setTimeout(function () { // comment this line to remove timeout
                    runningRequest = true;

                    request = APIClientCore.call({
                        type: 'GET',
                        url: options.ajaxUrl + val,
                        success: function (data) {
                            if (!$.isEmptyObject(data)) { // (or other) check for empty result
                                var appendList = '';
                                for (var key in data) {
                                    if (data.hasOwnProperty(key)) {
                                        var li = '';
                                        if (!!data[key].profile.images[0]) { // if image exists as in official docs
                                            li += '<li><a class="dropdown-link" href="/profile/' + data[key]._id + '" ><img src="' + data[key].profile.images[0].value + '" class="left">';
                                            li += "<span>" + highlight(data[key].name + " " + data[key].lastname, val) + "</span></a></li>";
                                        } else {
                                            li += '<li><a class="dropdown-link" href="/profile/' + data[key]._id + '" ><span>' + highlight(data[key].name + " " + data[key].lastname, val) + '</span></a></li>';
                                        }
                                        appendList += li;
                                    }
                                }
                                $autocomplete.append(appendList);
                            }else{
                                $autocomplete.append($('<li>No matches</li>'));
                            }
                        },
                        complete: function () {
                            runningRequest = false;
                        }
                    });
                }, 250);        // comment this line to remove timeout
            }
        });

        $(document).click(function (event) { // close ul if clicked outside
            if (!$(event.target).closest($autocomplete).length) {
                $autocomplete.empty();
            }
        });
    }
}
