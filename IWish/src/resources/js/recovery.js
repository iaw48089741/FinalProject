$(document).ready(function(){
    
    // makes sure both the password and password2 field are the same before
    // submitting the form
    $("#password2").on("keyup", function () {
        let password = $("#password").val();
        if (this.value != password) {
            this.setCustomValidity("The passwords don't match");
        } else {
            this.setCustomValidity("");
        }
    });

    $("#password").on("change", function () {
        $("#password2").trigger("keyup");
    });

});
