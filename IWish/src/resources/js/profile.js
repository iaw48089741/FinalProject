$(document).ready(function(){
	//prepare variables from pug
    userProfile = userProfile.replace(/&quot;/g, '"');

	//&lt; &gt;
    //userProfile = userProfile.replace(/(?:\r\n|\r|\n)/g, '<br>');

    userProfile = JSON.parse(userProfile);

    userSession = userSession.replace(/&quot;/g, '"');
    userSession = JSON.parse(userSession);

    posts = posts.replace(/&quot;/g, '"');

	var arrPosts = JSON.parse(posts);

	if (arrPosts.length == 0) {
	} else {

		//get last post date.
	    lastPostDate = arrPosts[arrPosts.length -1]._id.post.post_date;

	}


	//$('.carousel.carousel-slider').carousel({fullWidth: true});

	if(userSession.tutorial && userSession._id == userProfile._id) {
		setTimeout(function() { $('#modal-help').modal('open'); } , 100);
	}


	$('.photo-edit').click(function(){
		$('.form-picture').toggle();
	});


	$('.datepicker').pickadate({
		selectMonths: true, 
    	selectYears: 100,
		min: new Date(1900,01,01),
		// Mayor de 12 años
		max: new Date(new Date().getFullYear()-12, new Date().getMonth()+1, new Date().getDate()),
		today: 'Today',
		clear: 'Clear',
		close: 'OK'
	});

    // makes sure both the password1 and password2 field are the same before
    // submitting the form
    $("#password2").on("keyup", function () {
        let password = $("#password1").val();
        if (this.value != password) {
            this.setCustomValidity("The passwords don't match");
        } else {
            this.setCustomValidity("");
        }
    });

    $("#password1").on("change", function () {
        $("#password2").trigger("keyup");
    });

    // makes sure both localPassword1 and localPassword2 field are the same before
    // submitting the form
    $("#localPassword2").on("keyup", function () {
        let password = $("#localPassword1").val();
        if (this.value != password) {
            this.setCustomValidity("The passwords don't match");
        } else {
            this.setCustomValidity("");
        }
    });

    $("#localPassword1").on("change", function () {
        $("#localPassword2").trigger("keyup");
    });


	//Scrolling down
	$(window).scroll(function() {

		if($(window).scrollTop() + $(window).height() == $(document).height()) {
			getMorePostsScrolling(lastPostDate);
   		}

	});

	$("#title-post").on("change", function() {

		$("#title-post")[0].setCustomValidity("");

	});

	$("#content-post").on("change", function() {

		$("#title-post")[0].setCustomValidity("");

	});

	$("#file-post").on("change", function() {

		$("#title-post")[0].setCustomValidity("");

	});

	//uploadedImg1


})

//**** FUNCIONES ****//

/* HELP */
var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function getMorePostsScrolling() {

    APIClientCore.call({
        type: "GET",
        url: "/profile/getMorePosts/" + lastPostDate + "/" + userProfile._id,
        contentType: "application/json",
        success: function (data) {

        	data = data.d;
        	console.log(data);

        	//gen the HTML and append to posts container
        	var vHTML = "";

        	outer1: for (var i = 0; i < data.length; i++) {

        		//refresh last post date global variable
        		if (i == data.length -1) {

        			lastPostDate = data[i].post_date;

        		}

        		//post
        		vHTML += '<div class="card z-depth-4 hoverable">';
        		vHTML += '<div class="card-content">';
        		vHTML += '<div class="post-content">';
        		vHTML += '<p class="username">';
        		vHTML += data[i].username + " ";
        		vHTML += data[i].post_date.toString() + "</p>";
        		vHTML += "<h3>" + data[i].title + "</h3>";
        		vHTML += "<p>" + data[i].content + "</p></div>";

        		//edit form
        		vHTML += '<div class="post-content-edit" hidden="">';
        		vHTML += '<form action="/posts/edit/' + data[i]._id +'/0/0/' + userProfile._id + '" method="POST" enctype="multipart/form-data">';
        		vHTML += '<div class="row">';
        		vHTML += '<div class="input-field col s12">';
        		vHTML += '<label class="active" for="title-post">Title:</label>';
        		vHTML += '<input id="title-post" name="Title" placeholder="title" value="' + data[i].title + '" type="text">';
        		vHTML += '</div></div>';
        		vHTML += '<div class="row">';
        		vHTML += '<div class="input-field col s12">';
        		vHTML += '<label class="active" for="content-post">Text: </label>';
        		vHTML += '<textarea id="content-post" class="materialize-textaera" type="text" name="Description">' + data[i].content + "</textarea>";
        		vHTML += '</div></div>';
        		vHTML += '<img id="output" class="responsive-img" width="500">';
        		vHTML += '<input name="File" multiple="" type="file">';
        		vHTML += '<input class="btn" name="submit" value="edit" type="submit">';
        		vHTML += '</form>';
        		vHTML += '</div>';

        		//images
        		for (var ii = 0; ii < data[i].images.length; ii++) {

        			vHTML += '<div class="card-image">';
        			vHTML += '<div class="material-placeholder">';
        			vHTML += '<img class="materialboxed initialized" src="' + data[i].images[ii].path + '" data-caption="">';
        			vHTML += '</div>';
        			vHTML += '<span class="card-title"></span>';
        			vHTML += '</div>';

        		}

        		//buttons: only if we are friends. Or is my profile.
        		if ((userSession.id_friends.indexOf(userProfile._id) > -1 && userProfile.id_friends.indexOf(userSession._id) > -1) || userSession._id == userProfile._id) {

	        		vHTML += '<div class="buttons-post">';

	        		//buttons: like/unlike
	        		if (userSession.id_posts_like.indexOf(data[i]._id) == -1) {

	        			//like
		        		vHTML += '<a class="btn btn-custom halfway-fab waves-effect waves-light" alt="like" title="like" href="/posts/like/' + data[i]._id + '/0/0/' + userProfile._id + '">';
		        		vHTML += '<i class="material-icons">thumb_up</i>';
		        		vHTML += '</a>';

	        		} else {

		        		//unlike
		        		vHTML += '<a class="btn btn-custom halfway-fab waves-effect waves-light" alt="unlike" title="unlike" href="/posts/unlike/' + data[i]._id + '/0/0/' + userProfile._id + '">';
		        		vHTML += '<i class="material-icons">thumb_down</i>';
		        		vHTML += '</a>';

	        		}


	        		//buttons: reply
	        		vHTML += '<a class="btn btn-custom halfway-fab waves-effect waves-light btn-post" alt="reply" title="reply">';
	        		vHTML += '<i class="material-icons">textsms</i>';
	        		vHTML += '</a>';

	        		//buttons: edit
	        		if (userSession._id == data[i].id_user) {

	        			vHTML += '<a class="btn btn-custom halfway-fab waves-effect waves-light btn-edit-post" alt="edit" title="edit">';
		        		vHTML += '<i class="material-icons">mode_edit</i>';
		        		vHTML += '</a>';

	        		}

	        	}

        		//buttons: delete
        		if (userSession._id == data[i].id_user) {

        			vHTML += '<a class="btn btn-custom halfway-fab waves-effect waves-light btn-delete" href="#delete' + i + '" alt="delete" title="delete">';
	        		vHTML += '<i class="material-icons">delete</i>';
	        		vHTML += '</a>';

        		}

        		//buttons: recieve
        		if (userSession._id == userProfile._id && !data[i].recieved) {

        			vHTML += '<a class="btn btn-custom halfway-fab waves-effect waves-light btn-recieve right" href="#recieve' + i + '" alt="recieve" title="recieve">';
	        		vHTML += '<i class="material-icons">loyalty</i>';
	        		vHTML += '</a>';

	        		//modal buy
	        		vHTML += '<div id="recieve' + i + '" class="modal modal-recieve">';
	        		vHTML += '<div class="modal-content">';
	        		vHTML += '<h4>Recieve Item</h4>';
	        		vHTML += '<p>This action means you just recieved this item.</p>';
	        		vHTML += '<p>Everyone will be able to see that you recieved it in order to stop them from buying it again.</p>';
	        		vHTML += '<p>The people who were interested in this post will be notified.</p>';
	        		vHTML += '<p>Are you sure you want to continue?</p>';
	        		vHTML += '</div>';
	        		vHTML += '<div class="modal-footer">';
	        		vHTML += '<a class="modal-action modal-close waves-effect waves-green btn-flat" href="/posts/recieve/' + data[i]._id + '">Recieve</a>';
	        		vHTML += '<a class="modal-action modal-close waves-effect waves-green btn-flat" href="#">No</a>';
	        		vHTML += '</div>';
	        		vHTML += '</div>';

        		}

        		//buttons: only if we are friends.
        		if ((userSession.id_friends.indexOf(userProfile._id) > -1 && userProfile.id_friends.indexOf(userSession._id) > -1) || userSession._id == userProfile._id) {

	        		//buttons: interest
	        		if (userSession._id != data[i].id_user && userSession.id_interests.indexOf(data[i]._id) == -1 && !data[i].isBought && !data[i].recieved) {

	        			//interest
	        			vHTML += '<a class="btn btn-custom halfway-fab waves-effect waves-light btn-interest right" href="/posts/interest/' + data[i]._id + '" alt="interest" title="interest">';
		        		vHTML += '<i class="material-icons">tag_faces</i>';
		        		vHTML += '</a>';

	        		} else if (userSession.id_interests.indexOf(data[i]._id) > -1 && !data[i].isBought && !data[i].recieved) {

	        			//uninterest
						vHTML += '<a class="btn btn-custom halfway-fab waves-effect waves-light btn-uninterest right" href="/posts/uninterest/' + data[i]._id + '" alt="uninterest" title="uninterest">';
		        		vHTML += '<i class="material-icons">not_interested</i>';
		        		vHTML += '</a>';

	        		}

	        		//buttons: buy
	        		if (userSession.id_interests.indexOf(data[i]._id) > -1 && !data[i].isBought && !data[i].recieved) {

						//buy
						vHTML += '<a class="btn btn-custom halfway-fab waves-effect waves-light btn-buy right" href="#buy' + i + '" alt="buy" title="buy">';
		        		vHTML += '<i class="material-icons">shopping_cart</i>';
		        		vHTML += '</a>';

		        		//modal buy
		        		vHTML += '<div id="buy' + i + '" class="modal modal-buy">';
		        		vHTML += '<div class="modal-content">';
		        		vHTML += '<h4>Buy Item</h4>';
		        		vHTML += '<p>This action means you just bought this item for ' + userProfile.name + '.</p>';
		        		vHTML += '<p>The people who were interested in this post will be notified and you will remain anonymous.</p>';
		        		vHTML += '<p>Are you sure you want to continue?</p>';
		        		vHTML += '</div>';
		        		vHTML += '<div class="modal-footer">';
		        		vHTML += '<a class="modal-action modal-close waves-effect waves-green btn-flat" href="/posts/buy/' + data[i]._id + '">Buy</a>';
		        		vHTML += '<a class="modal-action modal-close waves-effect waves-green btn-flat" href="#">No</a>';
		        		vHTML += '</div>';
		        		vHTML += '</div>';

	        		}

	        		vHTML += '</div>'; //buttons post div

	        		//links modal users interested
	        		vHTML += '<div class="users-interested right">';

	        		for (var ij = 0; ij < data[i].users_interested.length; ij++) {

	        			if (ij > 3) { 
	        				vHTML += 'and more'
	        				continue outer1;
	        			}
	        			vHTML += '<a href="#modal-users-interested' + i + '" title="' + data[i].users_interested[ij].username + '">' + data[i].users_interested[ij].username;

	        			if (ij != data[i].users_interested.length -1) vHTML += ', ';

	        			vHTML += '</a>';

	        		}

	        		//modal users-interested
	        		vHTML += '<div id="modal-users-interested' + i + '" class="modal modal-users-interested">';
	        		vHTML += '<div class="modal-content">';
	        		vHTML += '<h4>Users interested</h4>';
	        		vHTML += '<ul class="collection">';
	        		
	        		//links to people that likes this post
	        		for (var ij = 0; ij < data[i].users_interested.length; ij++) {

	        			vHTML += '<li class="collection-item avatar">';
	        			vHTML += '<img class="circle" src="' + data[i].users_interested[ij].image + '" width="100">';
	        			vHTML += '<span class="Title">';
	        			vHTML += '<a href="/profile/' + data[i].users_interested[ij]._id + '">' + data[i].users_interested[ij].name + " " + data[i].users_interested[ij].lastname + " (" + data[i].users_interested[ij].username + ')</a>';
	        			vHTML += '</span>';

	        			if (data[i].users_interested[ij]._id != userSession._id) {

		        			if (userSession.id_friends.indexOf(data[i].users_interested[ij]._id) == -1) {

		        				vHTML += '<a class="right" href="/profile/addFriend/"' + data[i].users_interested[ij]._id + '">';
		        				vHTML += '<i class="material-icons">supervisor_account</i>add friend';
		        				vHTML += '</a>';

		        			} else {

		        				vHTML += '<a class="right" href="/profile/removeFriend/"' + data[i].users_interested[ij]._id + '">';
		        				vHTML += '<i class="material-icons">supervisor_account</i>remove friend';
		        				vHTML += '</a>';

		        			}

	        			}

	        			vHTML += '</li>';

	        		}

	        		vHTML += '</div>'; //modal-content
	        		vHTML += '<div class="modal-footer">';
	        		vHTML += '<a class="modal-action modal-close waves-effect waves-green btn" href="#">Close</a>';
	        		vHTML += '</div>'; //modal-footer
	        		vHTML += '</div>'; //modal

	        		vHTML += '</div>'; //users-interested

	        		//links modal people that likes this post
	        		vHTML += '<div class="users-like">';

	        		for (var ij = 0; ij < data[i].users_like.length; ij++) {

	        			if (ij > 3) { 
	        				vHTML += 'and more'
	        				continue outer1;
	        			}
	        			vHTML += '<a href="#modal-users-like' + i + '" title="' + data[i].users_like[ij].username + '">' + data[i].users_like[ij].username;

	        			if (ij != data[i].users_like.length -1) vHTML += ', ';

	        			vHTML += '</a>';

	        		}

	        		//modal users-like
	        		vHTML += '<div id="modal-users-like' + i + '" class="modal modal-users-like">';
	        		vHTML += '<div class="modal-content">';
	        		vHTML += '<h4>Users Like</h4>';
	        		vHTML += '<ul class="collection">';
	        		
	        		//links to people that likes this post
	        		for (var ij = 0; ij < data[i].users_like.length; ij++) {

	        			vHTML += '<li class="collection-item avatar">';
	        			vHTML += '<img class="circle" src="' + data[i].users_like[ij].image + '" width="100">';
	        			vHTML += '<span class="Title">';
	        			vHTML += '<a href="/profile/' + data[i].users_like[ij]._id + '">' + data[i].users_like[ij].name + " " + data[i].users_like[ij].lastname + " (" + data[i].users_like[ij].username + ')</a>';
	        			vHTML += '</span>';

	        			if (data[i].users_like[ij]._id != userSession._id) {

		        			if (userSession.id_friends.indexOf(data[i].users_like[ij]._id) == -1) {

		        				vHTML += '<a class="right" href="/profile/addFriend/"' + data[i].users_like[ij]._id + '">';
		        				vHTML += '<i class="material-icons">supervisor_account</i>add friend';
		        				vHTML += '</a>';

		        			} else {

		        				vHTML += '<a class="right" href="/profile/removeFriend/"' + data[i].users_like[ij]._id + '">';
		        				vHTML += '<i class="material-icons">supervisor_account</i>remove friend';
		        				vHTML += '</a>';

		        			}

	        			}

	        			vHTML += '</li>';

	        		}

	        		vHTML += '</div>'; //modal-content
	        		vHTML += '<div class="modal-footer">';
	        		vHTML += '<a class="modal-action modal-close waves-effect waves-green btn" href="#">Close</a>';
	        		vHTML += '</div>'; //modal-footer
	        		vHTML += '</div>'; //modal

	        		vHTML += '</div>'; //users-like

	        		//reply form
	        		vHTML += '<form class="form-post" action="/posts/' + data[i]._id +'/' + data[i].level_post + '/' + userProfile._id + '" method="POST" enctype="multipart/form-data" hidden="true">';
	        		vHTML += '<textarea class="materialize-textaera" type="text" name="Description" placeholder="Comment"></textarea>';
	        		vHTML += '<img id="output" class="responsive-img" width="500">';
	        		vHTML += '<input class="file-path validate" name="File" type="file">';
	        		vHTML += '<input class="btn col s3" name="submit" type="submit" value="submit">';
	        		vHTML += '</form>';


	        	}

        		//modal delete
        		vHTML += '<div id="delete' + i + '" class="modal modal-delete">';
        		vHTML += '<div class="modal-content">';
        		vHTML += '<h4>Delete Post</h4>';
        		vHTML += '<p>Are you sure you want to delete the post?</p>';
        		vHTML += '</div>';
        		vHTML += '<div class="modal-footer">';
        		vHTML += '<a class="modal-action modal-close waves-effect waves-green btn-flat" href="/posts/delete/' + data[i]._id + '/0/0/' + userProfile._id + '">Delete</a>';
        		vHTML += '<a class="modal-action modal-close waves-effect waves-green btn-flat" href="#">No</a>';
        		vHTML += '</div>';
        		vHTML += '</div>';

        		//replies
        		for (var j = 0; j < data[i].replies.length; j++) {

	        		//post
	        		vHTML += '<div class="card-content card-action hoverable"><div class="post-content"><p class="username">';
	        		vHTML += data[i].replies[j].username + " ";
	        		vHTML += data[i].replies[j].post_date.toString() + "</p>";
	        		vHTML += "<p>" + data[i].replies[j].content + "</p></div>";

	        		//edit form
	        		vHTML += '<div class="post-content-edit" hidden="">';
	        		vHTML += '<form action="/posts/edit/' + data[i]._id + '/' + data[i].replies[j]._id + '/0/' + userProfile._id + '" method="POST" enctype="multipart/form-data">';
	        		vHTML += '<textarea class="materialize-textaera" type="text" name="Description">' + data[i].replies[j].content + "</textarea>";
	        		vHTML += '<img id="output" class="responsive-img" width="500">';
	        		vHTML += '<input name="File" multiple="" type="file">';
	        		vHTML += '<input class="btn" name="submit" value="edit" type="submit">';
	        		vHTML += '</form>';
	        		vHTML += '</div>';

	        		//images
	        		for (var ji = 0; ji < data[i].replies[j].images.length; ji++) {

	        			vHTML += '<div class="card-image">';
	        			vHTML += '<img src="' + data[i].replies[j].images[ji].path + '">';
	        			vHTML += '</div>';

	        		}

	        		//buttons: only if we are friends. Or is my profile.
	        		if ((userSession.id_friends.indexOf(userProfile._id) > -1 && userProfile.id_friends.indexOf(userSession._id) > -1) || userSession._id == userProfile._id) {

		        		//buttons: like/unlike
		        		if (userSession.id_posts_like.indexOf(data[i].replies[j]._id) == -1) {

		        			//like
			        		vHTML += '<a class="btn btn-custom halfway-fab waves-effect waves-light" alt="like" title="like" href="/posts/like/' + data[i]._id + '/' + data[i].replies[j]._id + '/0/' + userProfile._id + '">';
			        		vHTML += '<i class="material-icons">thumb_up</i>';
			        		vHTML += '</a>';

		        		} else {

			        		//unlike
			        		vHTML += '<a class="btn btn-custom halfway-fab waves-effect waves-light" alt="unlike" title="unlike" href="/posts/unlike/' + data[i]._id + '/' + data[i].replies[j]._id + '/0/' + userProfile._id + '">';
			        		vHTML += '<i class="material-icons">thumb_down</i>';
			        		vHTML += '</a>';

		        		}

		        		//buttons: reply
		        		vHTML += '<a class="btn btn-custom halfway-fab waves-effect waves-light btn-reply" alt="reply" title="reply">';
		        		vHTML += '<i class="material-icons">textsms</i>';
		        		vHTML += '</a>';

		        		//buttons: edit
		        		if (userSession._id == data[i].replies[j].id_user) {

		        			vHTML += '<a class="btn btn-custom halfway-fab waves-effect waves-light btn-edit" alt="edit" title="edit">';
			        		vHTML += '<i class="material-icons">mode_edit</i>';
			        		vHTML += '</a>';

		        		}

		        	}

	        		//buttons: delete
	        		if (userSession._id == data[i].replies[j].id_user) {

	        			vHTML += '<a class="btn btn-custom halfway-fab waves-effect waves-light btn-delete" href="#delete-reply' + j + '" alt="delete" title="delete">';
		        		vHTML += '<i class="material-icons">delete</i>';
		        		vHTML += '</a>';

	        		}

	        		//buttons: only if we are friends. Or is my profile.
	        		if ((userSession.id_friends.indexOf(userProfile._id) > -1 && userProfile.id_friends.indexOf(userSession._id) > -1) || userSession._id == userProfile._id) {

		        		vHTML += '<div class="users-like">';

		        		//links to people that likes this post
		        		for (var jj = 0; jj < data[i].replies[j].users_like.length; jj++) {

		        			if (jj > 3) { 
		        				vHTML += 'and more';
		        				continue outer1;
		        			}
		        			vHTML += '<a href="#modal-users-like-reply' + j + '" title="' + data[i].replies[j].users_like[jj].username + '">' + data[i].replies[j].users_like[jj].username + '</a>';

		        			if (jj != data[i].replies[j].users_like.length -1) vHTML += ', ';

		        		}

		        		vHTML += '</div>'; //users-like

		        		//modal users-like
		        		vHTML += '<div id="modal-users-like-reply' + j + '" class="modal modal-users-like">';
		        		vHTML += '<div class="modal-content">';
		        		vHTML += '<h4>Users Like</h4>';
		        		vHTML += '<ul class="collection">';

		        		//links to people that likes this post
		        		for (var jj = 0; jj < data[i].replies[j].users_like.length; jj++) {

		        			vHTML += '<li class="collection-item avatar">';
		        			vHTML += '<img class="circle" src="' + data[i].replies[j].users_like[jj].image + '" width="100">';
		        			vHTML += '<span class="Title">';
		        			vHTML += '<a href="/profile/' + data[i].replies[j].users_like[jj]._id + '">' + data[i].replies[j].users_like[jj].name + " " + data[i].replies[j].users_like[jj].lastname + " (" + data[i].replies[j].users_like[jj].username + ')</a>';
		        			vHTML += '</span>';

		        			if (data[i].replies[j].users_like[jj]._id != userSession._id) {

			        			if (userSession.id_friends.indexOf(data[i].replies[j].users_like[jj]._id) == -1) {

			        				vHTML += '<a class="right" href="/profile/addFriend/"' + data[i].replies[j].users_like[jj]._id + '">';
			        				vHTML += '<i class="material-icons">supervisor_account</i>add friend';
			        				vHTML += '</a>';

			        			} else {

			        				vHTML += '<a class="right" href="/profile/removeFriend/"' + data[i].replies[j].users_like[jj]._id + '">';
			        				vHTML += '<i class="material-icons">supervisor_account</i>remove friend';
			        				vHTML += '</a>';

			        			}

		        			}

		        			vHTML += '</li>';

		        		}

		        		vHTML += '</div>'; //modal-content
		        		vHTML += '<div class="modal-footer">';
		        		vHTML += '<a class="modal-action modal-close waves-effect waves-green btn" href="#">Close</a>';
		        		vHTML += '</div>'; //modal-footer
		        		vHTML += '</div>'; //modal

		        		//reply form
		        		vHTML += '<form class="form-reply" action="/posts/' + data[i]._id + '/' + data[i].replies[j]._id + '/' + data[i].replies[j].level_post + '/' + userProfile._id + '" method="POST" enctype="multipart/form-data" hidden="true">';
		        		vHTML += '<textarea class="materialize-textaera" type="text" name="Description" placeholder="Comment"></textarea>';
		        		vHTML += '<img id="output" class="responsive-img" width="500">';
		        		vHTML += '<input class="file-path validate" name="File" type="file">';
		        		vHTML += '<input class="btn col s3" name="submit" type="submit" value="submit">';
		        		vHTML += '</form>';

		        	}

	        		//modal delete
	        		vHTML += '<div id="delete-reply' + j + '" class="modal modal-delete">';
	        		vHTML += '<div class="modal-content">';
	        		vHTML += '<h4>Delete Post</h4>';
	        		vHTML += '<p>Are you sure you want to delete the post?</p>';
	        		vHTML += '</div>';
	        		vHTML += '<div class="modal-footer">';
	        		vHTML += '<a class="modal-action modal-close waves-effect waves-green btn-flat" href="/posts/delete/' + data[i]._id + '/' + data[i].replies[j]._id + '/0/' + userProfile._id + '">Delete</a>';
	        		vHTML += '<a class="modal-action modal-close waves-effect waves-green btn-flat" href="#">No</a>';
	        		vHTML += '</div>';
	        		vHTML += '</div>';


	        		for (var k = 0; k < data[i].replies[j].replies.length; k++) {

						//post
		        		vHTML += '<div class="card-content card-action hoverable"><div class="post-content"><p class="username">';
		        		vHTML += data[i].replies[j].replies[k].username + " ";
		        		vHTML += data[i].replies[j].replies[k].post_date.toString() + "</p>";
		        		vHTML += "<p>" + data[i].replies[j].replies[k].content + "</p></div>";

		        		//edit form
		        		vHTML += '<div class="post-content-edit" hidden="">';
		        		vHTML += '<form action="/posts/edit/' + data[i]._id + '/' + data[i].replies[j]._id + '/' + data[i].replies[j].replies[k]._id + '/' + userProfile._id + '" method="POST" enctype="multipart/form-data">';
		        		vHTML += '<textarea class="materialize-textaera" type="text" name="Description">' + data[i].replies[j].replies[k].content + "</textarea>";
		        		vHTML += '<img id="output" class="responsive-img" width="500">';
		        		vHTML += '<input name="File" multiple="" type="file">';
		        		vHTML += '<input class="btn" name="submit" value="edit" type="submit">';
		        		vHTML += '</form>';
		        		vHTML += '</div>';

		        		//images
		        		for (var ki = 0; ki < data[i].replies[j].replies[k].images.length; ki++) {

		        			vHTML += '<div class="card-image">';
		        			vHTML += '<img src="' + data[i].replies[j].replies[k].images[ki].path + '">';
		        			vHTML += '</div>';

		        		}

		        		//buttons: only if we are friends. Or is my profile.
	        			if ((userSession.id_friends.indexOf(userProfile._id) > -1 && userProfile.id_friends.indexOf(userSession._id) > -1) || userSession._id == userProfile._id) {

			        		//buttons: like/unlike
			        		if (userSession.id_posts_like.indexOf(data[i].replies[j].replies[k]._id) == -1) {

			        			//like
				        		vHTML += '<a class="btn btn-custom halfway-fab waves-effect waves-light" alt="like" title="like" href="/posts/like/' + data[i]._id + '/' + data[i].replies[j]._id + '/' + data[i].replies[j].replies[k]._id + '/' + userProfile._id + '">';
				        		vHTML += '<i class="material-icons">thumb_up</i>';
				        		vHTML += '</a>';

			        		} else {

				        		//unlike
				        		vHTML += '<a class="btn btn-custom halfway-fab waves-effect waves-light" alt="unlike" title="unlike" href="/posts/unlike/' + data[i]._id + '/' + data[i].replies[j]._id + '/' + data[i].replies[j].replies[k]._id + '/' + userProfile._id + '">';
				        		vHTML += '<i class="material-icons">thumb_down</i>';
				        		vHTML += '</a>';

			        		}

			        		//buttons: edit
			        		if (userSession._id == data[i].replies[j].replies[k].id_user) {

			        			vHTML += '<a class="btn btn-custom halfway-fab waves-effect waves-light btn-edit" alt="edit" title="edit">';
				        		vHTML += '<i class="material-icons">mode_edit</i>';
				        		vHTML += '</a>';

			        		}

			        	}

		        		//buttons: delete
		        		if (userSession._id == data[i].replies[j].replies[k].id_user) {

		        			vHTML += '<a class="btn btn-custom halfway-fab waves-effect waves-light btn-delete" href="#delete-replyofreply' + k + '" alt="delete" title="delete">';
			        		vHTML += '<i class="material-icons">delete</i>';
			        		vHTML += '</a>';

		        		}

		        		//buttons: only if we are friends. Or is my profile.
	        			if ((userSession.id_friends.indexOf(userProfile._id) > -1 && userProfile.id_friends.indexOf(userSession._id) > -1) || userSession._id == userProfile._id) {

			        		vHTML += '<div class="users-like">';

			        		//links to people that likes this post
			        		for (var kj = 0; kj < data[i].replies[j].replies[k].users_like.length; kj++) {

			        			if (kj > 3) { 
			        				vHTML += 'and more';
			        				continue outer1;
			        			}
			        			vHTML += '<a href="#modal-users-like-reply' + k + '" title="' + data[i].replies[j].replies[k].users_like[kj].username + '">' + data[i].replies[j].replies[k].users_like[kj].username + '</a>';

			        			if (kj != data[i].replies[j].replies[k].users_like.length -1) vHTML += ', ';

			        		}

			        		vHTML += '</div>'; //users-like

			        		//modal users-like
			        		vHTML += '<div id="modal-users-like-reply' + k + '" class="modal modal-users-like">';
			        		vHTML += '<div class="modal-content">';
			        		vHTML += '<h4>Users Like</h4>';
			        		vHTML += '<ul class="collection">';

			        		//links to people that likes this post
			        		for (var kj = 0; kj < data[i].replies[j].replies[k].users_like.length; kj++) {

			        			vHTML += '<li class="collection-item avatar">';
			        			vHTML += '<img class="circle" src="' + data[i].replies[j].replies[k].users_like[kj].image + '" width="100">';
			        			vHTML += '<span class="Title">';
			        			vHTML += '<a href="/profile/' + data[i].replies[j].replies[k].users_like[kj]._id + '">' + data[i].replies[j].replies[k].users_like[kj].name + " " + data[i].replies[j].replies[k].users_like[kj].lastname + " (" + data[i].replies[j].replies[k].users_like[kj].username + ')</a>';
			        			vHTML += '</span>';

			        			if (data[i].replies[j].users_like[kj]._id != userSession._id) {

				        			if (userSession.id_friends.indexOf(data[i].replies[j].users_like[kj]._id) == -1) {

				        				vHTML += '<a class="right" href="/profile/addFriend/"' + data[i].replies[j].users_like[kj]._id + '">';
				        				vHTML += '<i class="material-icons">supervisor_account</i>add friend';
				        				vHTML += '</a>';

				        			} else {

				        				vHTML += '<a class="right" href="/profile/removeFriend/"' + data[i].replies[j].users_like[kj]._id + '">';
				        				vHTML += '<i class="material-icons">supervisor_account</i>remove friend';
				        				vHTML += '</a>';

				        			}

			        			}

			        			vHTML += '</li>';

			        		}

			        		vHTML += '</div>'; //modal-content
			        		vHTML += '<div class="modal-footer">';
			        		vHTML += '<a class="modal-action modal-close waves-effect waves-green btn" href="#">Close</a>';
			        		vHTML += '</div>'; //modal-footer
			        		vHTML += '</div>'; //modal

			        	}

		        		//modal delete
		        		vHTML += '<div id="delete-replyofreply' + k + '" class="modal modal-delete">';
		        		vHTML += '<div class="modal-content">';
		        		vHTML += '<h4>Delete Post</h4>';
		        		vHTML += '<p>Are you sure you want to delete the post?</p>';
		        		vHTML += '</div>';
		        		vHTML += '<div class="modal-footer">';
		        		vHTML += '<a class="modal-action modal-close waves-effect waves-green btn-flat" href="/posts/delete/' + data[i]._id + '/' + data[i].replies[j]._id + '/' + data[i].replies[j].replies[k]._id + '/' + userProfile._id + '">Delete</a>';
		        		vHTML += '<a class="modal-action modal-close waves-effect waves-green btn-flat" href="#">No</a>';
		        		vHTML += '</div>';
		        		vHTML += '</div>';

		        		//closing tags
		        		vHTML += '</div>';

		        	}

		        	vHTML += '</div>';

        		}

		        vHTML += '</div>';
        		vHTML += '</div>';


        	}

        	$("#postsContainer").append(vHTML);

        	//attach all events to these posts.
        	InitEvents(false);

        }
    });

}
