const path  = require('path');
const crypto= require("crypto");
const sharp	= require("sharp");
const fs 	= require("fs");
const mongoose = require("mongoose");
//const Promise = require("promise");
const nodemailer = require("nodemailer");

const Image = require("../../../model/image.js");
const User  = require("../../../model/user.js");

var smtpTransport = nodemailer.createTransport("SMTP", {
    service: "Gmail",
    auth: {
        user: "noreply.iwish@gmail.com",
        pass: "1W15h!B3niS*"
    }
});



var getProfilePosts = function(params, callback){

	//cargar posts recientes
	User.aggregate([
		{$match: {"_id": params.user._id}},
		{$unwind: "$posts"},
		{$group: {"_id":{ post: "$posts"}}},
		{$sort: {"_id.post.post_date": -1 }},
		{$limit:6}
		], 
		callback);
}

var getMoreProfilePosts = function(params, callback){

	//load all posts
	User.aggregate([
		{$match: {"_id": mongoose.Types.ObjectId(params.idUserProfile)}},
		{$unwind: "$posts"},
		//{$group: {"_id":{ post: "$posts"}}},
		{$project: { "_id": 0, "post": "$posts" } },
		{$sort: {"post.post_date": -1 }},
		{$match: {"post.post_date": {$lt: new Date(params.lastPostDate)}}},
		{$limit: params.limit}
		], 
		function(err, posts) {
			if (err) return callback(err);
			var arrPosts = [];
			for (var i = 0; i < posts.length; i++) {
				arrPosts.push(posts[i].post);
			}
			callback(false, arrPosts);
		});

}

function getProfileUserById(params) {
	//retornamos una promise.
	return new Promise(function(resolve, reject) {

		User.findOne({ _id: params.idUser },function(err, user) {
			if (err) return reject(err);
			resolve(user);

		});

	});

}

function addProfilePic(params, callback) {

	var user = params.user;

	if (params.files.File) {

		var isImage = false;

		if (params.files.File.mimetype == "image/png") isImage = true;
		if (params.files.File.mimetype == "image/jpeg") isImage = true;

		if (isImage) {

			//filtramos calidad y convertimos a jpeg
			var imagePromise = sharp(params.files.File.data);
			var imagePromise2 = imagePromise.metadata().then(function(metadata) {
				return imagePromise.png({quality: 90}).toBuffer()
			}).then(function(data) {
				//usamos un hash para no repetir nunca una imagen
				//despues del filtro.
				var hash = crypto.createHash("sha256");
				hash.update(data);
				var hashStr = hash.digest("hex");
			 	var new_image = new Image();
				var image_path = "../user_images/";

				new_image.path = image_path + hashStr + ".png";
				new_image.value = new_image.path; //para hacer el profile.pug mas facil
				new_image.upload_date = new Date();

				user.profile.images.push(new_image);

				fs.writeFile(new_image.path, data, function(err) {
	            	if (err) return callback(err);
	            });

				user.save(function(err) {
					return callback(err);
				});

			});
			
		} else {

			return callback({ error: 101, errorMessage: "Unsupported file format."});

		}

	} else {

		return callback();

	}

}

function addNewFriend(params, callback) {

	var user = params.user;

	user.id_friends.push(params.idNewFriend);

	User.findById(params.idNewFriend, function(err, newFriend) {

		if (err) return callback(err);

		//send mail to new friend
		var link = "localhost/profile/" + user._id;

		var mailOptions = {
            to : newFriend.email,
            subject : user.username + " added you to his/her iWish friendlist!",
            html : "<p>Hello " + newFriend.username + ".</p><p>Good news! <a href=" + link + ">" + user.username + "</a> wants to make your wishes come true. Add him/her to your friendlist and have fun!</p>"
	    }
	    
		smtpTransport.sendMail(mailOptions, function(error, response){

	        if (error) {
	            return callback(error, response);
	        } 

			user.save(function(err) {
				return callback(err);
			});

	    });

	});


}

function removeFriend(params, callback) {

	var user = params.user;

	user.id_friends.pull(params.idFriend);
	user.save(function(err) {
		callback(err);
	});

}

function getUserFriendList(params, callback) {

	var user;
	if (params.useUserSession) {
		user = params.userSession;
	} else {
		user = params.user;
	}
	var friends = [];
	var PromisesArr = [];

	for (var i = 0; i < user.id_friends.length; i++) {

		var vPromise = User.findById(user.id_friends[i], function(err, friend) {
			if (err) return callback(err);
			friends.push(friend);

		});

		PromisesArr.push(vPromise);

	}

	Promise.all(PromisesArr).then(function() {
		callback(false, friends);
	});

}

function getSuggestedUsers(params, callback) {

	var user;
	if (params.useUserSession) {
		user = params.userSession;
	} else {
		user = params.user;
	}

	var suggestions = [];

	User.find({}, function(err, suggestedUsers) {
		if (err) return callback(err);

		for (var i = 0; i < params.nSuggestions; i++) {

			if (suggestedUsers[i]) {
				if (!user._id.equals(suggestedUsers[i]._id) && user.id_friends.indexOf(suggestedUsers[i]._id) == -1)
				suggestions.push(suggestedUsers[i]);
			}
				

		}	 
		console.log(suggestions);
		callback(false, suggestions);

	});

}

function disableTutorial(params, callback) {
	console.log(params.tutorial);
	var user = params.user;
	if(params.tutorial){	
		user.tutorial = false;
		user.save(function(err) {
			return callback(err);
		});
	} else {
		console.log("WYEPI");
		return callback(false);
	}

}

module.exports = { 
	getProfilePosts: getProfilePosts,
	getProfileUserById: getProfileUserById,
	addProfilePic: addProfilePic,
	addNewFriend: addNewFriend,
	getUserFriendList: getUserFriendList,
	removeFriend: removeFriend,
	getMoreProfilePosts: getMoreProfilePosts,
	getSuggestedUsers: getSuggestedUsers,
	disableTutorial: disableTutorial
}	

