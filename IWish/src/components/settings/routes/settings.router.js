const settingsController = require("../controller/settings.controller.js");
const User  = require("../../../model/user.js");
const Promise = require("promise");
const path  = require('path');
const PATH_ERROR = path.resolve("../views/error/error.pug");

module.exports = function(app) {

	app.post("/settings/update", function(req, res) {

		if (req.isAuthenticated()) {

	    	var params = {};
			params.user = req.user;
			params.newName = req.body.name;
			params.newLastname = req.body.lastname;
			params.newUsername = req.body.username;
			params.newBirth = req.body.birth;
			params.password = req.body.password1;
            params.confirmPassword = req.body.password2;
            params.currentPassword = req.body.currentPassword;

			settingsController.updateSettings(params, function(err) {

				if (err) return res.render(PATH_ERROR);
				res.redirect(req.get('referer'));
				
			});
    		
		} else {
			res.render('../views/login/login.pug');
		}

	});


}
