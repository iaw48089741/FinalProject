const path  = require('path');
const nodemailer = require("nodemailer");
const Post 	= require("../../../model/post.js");
const Image = require("../../../model/image.js");
const User  = require('../../../model/user.js');
const crypto= require("crypto");
const sharp	= require("sharp");
const fs 	= require("fs");
const Promise = require("promise");

//const ImageMagick = require("imagemagick");

var smtpTransport = nodemailer.createTransport("SMTP", {
    service: "Gmail",
    auth: {
        user: "noreply.iwish@gmail.com",
        pass: "1W15h!B3niS*"
    }
});



function postPost(params, callback) {

	var user = params.user;
	var postOwner = params.postOwner;
	var new_post = new Post();
	new_post.images = [];
	var PromisesArray = [];


	new_post.id_user = user._id;
	new_post.username = user.username;
	new_post.title = params.body.Title;
	new_post.content = params.body.Description;

	//new lines
	//new_post.content = new_post.content.replace(/(?:\r\n|\r|\n)/g, '<br>');

	new_post.post_date = new Date();
	new_post.edit_date = new Date();
	user.id_posts.push(new_post._id);

	//si ha subido imagen
	if (params.files.File) {

		var isImage = false;

		//FIXME
		//for (params.files.length) {
			//comprovar que es una imagen... mirar todos los tipos aceptables
			if (params.files.File.mimetype == "image/png") isImage = true;
			if (params.files.File.mimetype == "image/jpeg") isImage = true;

			if (isImage) {

				//falta for each si suben mas de 1 imagen
				//filtramos calidad y convertimos a jpeg
				var imagePromise = sharp(params.files.File.data);
				var imagePromise2 = imagePromise.metadata().then(function(metadata) {
					//return imagePromise.jpeg({quality: 90}).toBuffer()
					return imagePromise.png().toBuffer();
				}).then(function(data) {
					//usamos un hash para no repetir nunca una imagen
					//despues del filtro.
					var hash = crypto.createHash("sha256");
					hash.update(data);
					var hashStr = hash.digest("hex");
				 	var new_image = new Image();
					var image_path = "../user_images/";

					//new_image.path = image_path + hashStr + ".jpg";
					new_image.path = image_path + hashStr + ".png";
					new_image.upload_date = new Date();

					new_post.images.push(new_image);

					fs.writeFile(new_image.path, data, function(err) {
	                	if (err) return err;
	                });

				});

				PromisesArray.push(imagePromise2);
				
			} else {

				return callback({ error: 101, errorMessage: "Unsupported format file."})

			}
		//}

		Promise.all(PromisesArray).then(function(){

			switch (params.postLevel + "") {
				case "0":

					new_post.level_post = 1;
					user.posts.push(new_post);

					user.save(function(err) {
						callback(err, new_post);
					});

					break;

				case "1":

					//si es un reply a el post de otra persona, hay que hacer 2 operaciones, sino sólo una

					new_post.level_post = 2;

					if (user._id.equals(params.idUserProfile)) {

						User.updateOne({"posts._id" : params.idPost },{ "$push" : { "posts.$.replies": new_post, "id_posts": new_post._id }}, function(err, result) {
							
							user.save(function(err) {
								callback(err, new_post);
							});

						});

					} else {

						User.updateOne({"posts._id" : params.idPost },{ "$push" : { "posts.$.replies": new_post }}, function(err, result) {
							
							user.save(function(err) {
								callback(err, new_post);
							});

						});

					}

					//var prom2 = User.updateOne({"id_posts" : params.idPost},{ "$push" : { "id_posts": new_post._id }});
					break;

				case "2":

					new_post.level_post = 3;
					
					User.findById(params.idUserProfile, function(err, data) {

						if (err) throw err;

						data.posts.id(params.idPost)
							.replies.id(params.idReply)
							.replies
							.push(new_post);

						if (user._id.equals(params.idUserProfile)) {

							data.id_posts.push(new_post._id);
							data.save(function(err) {

								callback(err, new_post);

							});

						} else {

							data.save(function(err) {

								user.save(function(err) {
									callback(err, new_post);
								});

							});

						}

					});

					break;

				default:
				//no se deberia llegar aqui.
			}

		});

	} else {
		//post sin imagenes

		//según lo anidado que esté el post, se usa un método más adecuado para hacer la operación
		switch (params.postLevel + "") {
			case "0":

				new_post.level_post = 1;
				user.posts.push(new_post);

				user.save(function(err) {
					callback(err, new_post);
				});

				break;

			case "1":

				//si es un reply a el post de otra persona, hay que hacer 2 operaciones, sino sólo una

				new_post.level_post = 2;

				if (user._id.equals(params.idUserProfile)) {

					User.updateOne({"posts._id" : params.idPost },{ "$push" : { "posts.$.replies": new_post, "id_posts": new_post._id }}, function(err, userUpdated) {
						
						user.save(function(err) {
							callback(err, new_post);
						});

					});

				} else {

					User.updateOne({"posts._id" : params.idPost },{ "$push" : { "posts.$.replies": new_post }}, function(err, userUpdated) {
						
						user.save(function(err) {
							callback(err, new_post);
						});

					});

				}

				//var prom2 = User.updateOne({"id_posts" : params.idPost},{ "$push" : { "id_posts": new_post._id }});
				break;

			case "2":

				new_post.level_post = 3;
				
				User.findById(params.idUserProfile, function(err, data) {

					if (err) throw err;

					data.posts.id(params.idPost)
						.replies.id(params.idReply)
						.replies
						.push(new_post);

					if (user._id.equals(params.idUserProfile)) {

						data.id_posts.push(new_post._id);
						data.save(function(err) {

							callback(err, new_post);

						});

					} else {

						data.save(function(err) {

							user.save(function(err) {
								callback(err, new_post);
							});

						});

					}

				});

				break;

			default:
				//no se deberia llegar aqui.
		}
	}
}


//FIXME
function updatePost(params, callback) {

	var imagesArray = [];
	var PromisesArray = [];
	//si ha subido imagen
	if (params.files.File) {

		var isImage = false;

		//for (params.files.length) {
			//comprovar que es una imagen... mirar todos los tipos aceptables
			if (params.files.File.mimetype == "image/png") isImage = true;
			if (params.files.File.mimetype == "image/jpeg") isImage = true;

			if (isImage) {

				//falta for each si suben mas de 1 imagen
				//filtramos calidad y convertimos a jpeg
				var imagePromise = sharp(params.files.File.data);
				var imagePromise2 = imagePromise.metadata().then(function(metadata) {
					//return imagePromise.jpeg({quality: 90}).toBuffer()
					return imagePromise.png().toBuffer();
				}).then(function(data) {

					//usamos un hash para no repetir nunca una imagen
					//despues del filtro.
					var hash = crypto.createHash("sha256");
					hash.update(data);
					var hashStr = hash.digest("hex");
				 	var new_image = new Image();
					var image_path = "../user_images/";

					//new_image.path = image_path + hashStr + ".jpg";
					new_image.path = image_path + hashStr + ".png";
					new_image.upload_date = new Date();

					imagesArray.push(new_image);

					fs.writeFile(new_image.path, data, function(err) {
						console.log("err: " + err);
	                	if (err) return err;
	                	console.log("not err");
	                });

				});

				PromisesArray.push(imagePromise2);

			} else {

				return callback({ error: 101, errorMessage: "Unsupported format file."})

			}


		//}
		Promise.all(PromisesArray).then(function() {

			//update post de primer nivel
			if (params.idReply == "0") {

				User.updateOne({"posts._id" : params.idPost },
					{"$set" :
						{ "posts.$.title": params.newTitle,
						  "posts.$.content": params.newDescription,
						  "posts.$.edit_date": new Date(),
						  "posts.$.images": imagesArray
						}
					},
					function(err, updated) {

						callback(err, updated);

				});

			//update post de segundo nivel
			} else if (params.idReplyOfReply == "0") {

				User.findById(params.idUserProfile, function(err, data) {

					if (err) throw err;
					data.posts.id(params.idPost)
						.replies.id(params.idReply)
						.content = params.newDescription;
					data.posts.id(params.idPost)
						.replies.id(params.idReply)
						.images = imagesArray;
					data.save(function(err) {
						callback(err, new_post);
					});

				});

			//update post de tercer nivel
			} else {

				User.findById(params.idUserProfile, function(err, data) {

					if (err) throw err;
					data.posts.id(params.idPost)
						.replies.id(params.idReply)
						.replies.id(params.idReplyOfReply)
						.content = params.newDescription;
					data.posts.id(params.idPost)
						.replies.id(params.idReply)
						.replies.id(params.idReplyOfReply)
						.images = imagesArray;
					data.save(function(err) {
						callback(err, new_post);
					});

				});

			}
        	

        	
		});

	//no ha subido imagen
	} else {

		//update post de primer nivel
			if (params.idReply == "0") {

				User.updateOne({"posts._id" : params.idPost },
					{"$set" :
						{ "posts.$.title": params.newTitle,
						  "posts.$.content": params.newDescription,
						  "posts.$.edit_date": new Date()
						}
					},
					function(err, updated) {

						callback(err, updated);

				});

			//update post de segundo nivel
			} else if (params.idReplyOfReply == "0") {

				User.findById(params.idUserProfile, function(err, data) {

					if (err) throw err;
					data.posts.id(params.idPost)
						.replies.id(params.idReply)
						.content = params.newDescription;
					data.save(function(err) {
						callback(err);
					});

				});

			//update post de tercer nivel
			} else {

				User.findById(params.idUserProfile, function(err, data) {

					if (err) throw err;
					data.posts.id(params.idPost)
						.replies.id(params.idReply)
						.replies.id(params.idReplyOfReply)
						.content = params.newDescription;
					data.save(function(err) {
						callback(err);
					});

				});

			}
	}
	

}

function deletePost(params, callback) {

	//delete post de primer nivel
	if (params.idReply == "0") {

		User.findById(params.idUserProfile, function(err, user) {
			
			user.posts.id(params.idPost).remove();
			user.save(function (err) {
				callback(err);
			});

		});

	//delete post de segundo nivel
	} else if (params.idReplyOfReply == "0") {

		User.findById(params.idUserProfile, function(err, user) {

			user.posts.id(params.idPost)
				.replies.id(params.idReply).remove();
			user.save(function(err) {
				callback(err);
			});

		});

	//delete post de tercer nivel
	} else {

		User.findById(params.idUserProfile, function(err, user) {

			user.posts.id(params.idPost)
				.replies.id(params.idReply)
				.replies.id(params.idReplyOfReply).remove();
			user.save(function(err) {
				callback(err);
			});

		});

	}

}

function getPostOwner(params) {

	return new Promise(function(resolve, reject) {

		//si es un replyOfReply
		if (params.idReplyOfReply + "" != "0" && typeof params.idReplyOfReply != "undefined") {

			User.findOne({ id_posts: params.idReplyOfReply },function(err, user) {
				if (err) reject(err);
				resolve(user);

			});

			return;

		}
		//si es un reply
		if (params.idReply + "" != "0" && typeof params.idReply != "undefined") {
			User.findOne({ id_posts: params.idReply },function(err, user) {
				if (err) reject(err);
				resolve(user);

			});

			return;

		} else {

			User.findOne({ id_posts: params.idPost },function(err, user) {
				if (err) reject(err);
				resolve(user);

			});

			return;

		}

	});

}


function interestPost(params, callback) {

	var user = params.user;
	var idPost = params.idPost;
	var postOwner = params.postOwner;

	if (user._id.equals(postOwner._id)) return callback(true);

	user.id_interests.push(idPost);
	user.save(function(err) {
		if (err) return callback(err);
	});

	postOwner.posts.id(idPost)
		.users_interested.push({_id: user._id, username: user.username, name: user.name, lastname: user.lastname, image: user.profile.images[user.profile.images.length - 1].value, email: user.email});
	postOwner.save(function(err) {
		callback(err);
	});
		
}

function uninterestPost(params, callback) {

	var user = params.user;
	var idPost = params.idPost;
	var postOwner = params.postOwner;

	user.id_interests.pull(idPost);
	user.save(function(err) {
		if (err) return;
	});

	postOwner.posts.id(idPost)
		.users_interested.pull({_id: user._id});
	postOwner.save(function(err) {
		callback(err);
	});	
	
}

function likePost(params, callback) {

	var user = params.user;
	var idPost = params.idPost;
	var idReply = params.idReply;
	var idReplyOfReply = params.idReplyOfReply;
	var idUserProfile = params.idUserProfile;

	//if the user is the profile owner, we'll save it later.
	if (idUserProfile != user._id) {

		if (idReply + "" == "0") user.id_posts_like.push(idPost);
			
		else if (idReplyOfReply + "" == "0") user.id_posts_like.push(idReply);

		else user.id_posts_like.push(idReplyOfReply);

		user.save(function(err) {
			if (err) return callback(err, { });
		});

	}

	if (idReply + "" == "0") {

		User.findById(idUserProfile, function(err, data) {

			data.posts.id(idPost)
				.users_like.push({_id: user._id, username: user.username, name: user.name, lastname: user.lastname, image: user.profile.images[user.profile.images.length - 1].value});

			if (idUserProfile == user._id) data.id_posts_like.push(idPost);

			data.save(function(err) {
				callback(err, data);
			});

		});

	} else if (idReplyOfReply + "" == "0") {

		User.findById(idUserProfile, function(err, data) {
			
			data.posts.id(idPost).replies.id(idReply)
				.users_like.push({_id: user._id, username: user.username, name: user.name, lastname: user.lastname, image: user.profile.images[user.profile.images.length - 1].value});

			if (idUserProfile == user._id) data.id_posts_like.push(idReply);

			data.save(function(err) {
				callback(err, data);
			});

		});

	} else {

		User.findById(idUserProfile, function(err, data) {
			
			data.posts.id(idPost).replies.id(idReply).replies.id(idReplyOfReply)
				.users_like.push({_id: user._id, username: user.username, name: user.name, lastname: user.lastname, image: user.profile.images[user.profile.images.length - 1].value});

			if (idUserProfile == user._id) data.id_posts_like.push(idReplyOfReply);

			data.save(function(err) {
				callback(err, data);
			});

		});

	}	
	
}

function unlikePost(params, callback) {

	var user = params.user;
	var idPost = params.idPost;
	var idReply = params.idReply;
	var idReplyOfReply = params.idReplyOfReply;
	var idUserProfile = params.idUserProfile;


	//if the user is the profile owner, we'll save it later.
	if (idUserProfile != user._id) {

		if (idReply + "" == "0") {

			user.id_posts_like.pull(idPost);

		} else if (idReplyOfReply + "" == "0") {

			user.id_posts_like.pull(idReply);

		} else {

			user.id_posts_like.pull(idReplyOfReply);

		}

		user.save(function(err) {

			if (err) return callback(err, { });

		});
	}

	if (idReply + "" == "0") {

		User.findById(idUserProfile, function(err, data) {

			data.posts.id(idPost).users_like.pull({ _id: user._id });

			if (idUserProfile == user._id) data.id_posts_like.pull(idPost);

			data.save(function(err) {
				callback(err, data);
			});

		});

	} else if (idReplyOfReply + "" == "0") {

		User.findById(idUserProfile, function(err, data) {

			data.posts.id(idPost).replies.id(idReply)
				.users_like.pull({ _id: user._id });

			if (idUserProfile == user._id) data.id_posts_like.pull(idReply);

			data.save(function(err) {
				callback(err, data);
			});

		});

	} else {

		User.findById(idUserProfile, function(err, data) {

			data.posts.id(idPost).replies.id(idReply).replies.id(idReplyOfReply)
				.users_like.pull({ _id: user._id });

			if (idUserProfile == user._id) data.id_posts_like.pull(idReplyOfReply);

			data.save(function(err) {
				callback(err, data);
			});

		});

	}
	
}

function buyPost(params, callback) {

	var user = params.user;
	var idPost = params.idPost;
	var postOwner = params.postOwner;

	user.id_posts_bought.push(idPost);
	user.save(function(err) {
		if (err) return callback(err);
	});


	postOwner.posts.id(idPost)
		.isBought = true;
	postOwner.save(function(err) {
		callback(err);
	});
	
}

/**
 * Envia email a todos los usuarios interesados cuando alquien le da a comprado
 */
function sendMailBought(params, callback) {
	
	var idPost = params.idPost;
	var postOwner = params.postOwner;

	var	users_interested = postOwner.posts.id(idPost).users_interested;
	var link = "localhost/profile/"+postOwner._id;
	// Get emails from people who was interested.
	var emails = [];

	for (var i = 0; i < users_interested.length; i++) {

		emails.push(users_interested[i].email);

	}

	var title = postOwner.posts.id(idPost).title;
	var content = postOwner.posts.id(idPost).content;

	var mailOptions = {
            to : emails,
            subject : "An Iwish present you we're interested in has been bought.",
            html : "<p>Someone was faster than you and bought that present for " + postOwner.username + ",</p><p> check <a href=" + link + ">his wishlist</a> to see if you can buy him another one!</p>"+
            "<div><h4>" + title + "</h4><p>" + content + "</p></div>"
    }
    
	smtpTransport.sendMail(mailOptions, function(error, response){
        if (error) {
            return callback(error, response);
        } 
        return callback(null, response);
    });

}

/**
 * Envia email a todos los usuarios interesados cuando se ha recibido el regalo
 */
function sendMailRecieved(params, callback) {
	
	var idPost = params.idPost;
	var postOwner = params.postOwner;

	var	users_interested = postOwner.posts.id(idPost).users_interested;

	//si no hay interesados return
	if (users_interested.length == 0) return callback();

	var link = "localhost/profile/"+postOwner._id;
	// Get emails from people who was interested.
	var emails = [];

	for (var i = 0; i < users_interested.length; i++) {

		emails.push(users_interested[i].email);

	}

	var title = postOwner.posts.id(idPost).title;
	var content = postOwner.posts.id(idPost).content;

	var mailOptions = {
            to : emails,
            subject : "An Iwish present you we're interested in has been recieved.",
            html : "<p>Your friend  " + postOwner.username + " will be now happy that he has recieved his wish!,</p><p> check <a href=" + link + ">his wishlist</a> to see if you can buy him another one!</p>"+
            "<div><h4>" + title + "</h4><p>" + content + "</p></div>"
    }
    
	smtpTransport.sendMail(mailOptions, function(error, response){
        if (error) {
            return callback(error, response);
        } 
        return callback(null, response);
    });

}

function recievePost(params, callback) {

	var idPost = params.idPost;
	var postOwner = params.postOwner;

	postOwner.posts.id(idPost)
		.recieved = true;
	postOwner.save(function(err) {
		callback(err);
	});

}

module.exports = {
	postPost: postPost,
	getPostOwner: getPostOwner,
	updatePost: updatePost,
	deletePost: deletePost,
	likePost: likePost,
	unlikePost: unlikePost,
	interestPost: interestPost,
	uninterestPost: uninterestPost,
	buyPost: buyPost,
	sendMailBought: sendMailBought,
	recievePost: recievePost,
	sendMailRecieved: sendMailRecieved
}
