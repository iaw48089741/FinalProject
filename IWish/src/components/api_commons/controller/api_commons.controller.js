const jwt    = require("jsonwebtoken");
const User   = require("../../../model/post.js");

// A keyboard-mashing generated pseudorandom string
const APP_SECRET     = "O432rfIUHFghgf32IO/S)¡'5ah($·h$KJHKfdsjh321jlk";
// Time (in seconds) for which a token should remain valid
const TOKEN_LIFETIME = 43200;
// Default signing algorithm
const SIGN_ALGORITHM = "HS512";

function genToken(params, callback) {
    var iss_id = params.id_issuer
    var user_id = params.id_user;
    var current_timestamp = Math.floor(Date.now() / 1000);
    
    var jwtHeader = {
        alg: SIGN_ALGORITHM,
        typ: "JWT"
    };
    var jwtPayload = {
        iss: iss_id,
        sub: user_id,
        iat: current_timestamp,
        exp: current_timestamp + TOKEN_LIFETIME
    };
    
    jwt.sign(jwtPayload, APP_SECRET,
             { algorithm: SIGN_ALGORITHM,
               header:    jwtHeader       },
             callback);
}

function verifyToken(params, callback) {
    var token = params.token;

    jwt.verify(token, APP_SECRET,
               { algorithms:     [SIGN_ALGORITHM],
                 clockTolerance: 30                },
               callback);

}

module.exports = {
    genToken: genToken,
    verifyToken: verifyToken
}
