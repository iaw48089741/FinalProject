const apiCommons = require("../controller/api_commons.controller.js");

// Id for the web API issuer
const WEBAPP_ISS_ID = 0;

module.exports = function (app) {

    /*
     * Queries the main app, where Passport is enabled, to emit
     * a new JWT for this client.
     *
     * This is filthy cheating to avoid implementing refresh tokens,
     * but who cares
     * */
    app.get("/authorization/jwt", function (req, res) {
        if (req.isAuthenticated()) {
            var params = {};
            params.id_user = req.user._id;
            params.id_issuer = WEBAPP_ISS_ID;

            apiCommons.genToken(params, function (err, token) {
                if (err) {
                    return res.json({ 
                        error: 101, 
                        errorMessage: "Could not generate token" 
                    });
                }
                
                return res.json({ jwt: token });
            });
        } else {
            return res.json({ 
                error: 1, 
                errorMessage: "You are not logged in" 
            });
        }
    });

}
