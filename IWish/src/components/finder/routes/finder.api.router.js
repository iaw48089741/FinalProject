const finderController = require("../controller/finder.controller.js");

module.exports = function(app) {

	/**
	 * Router de buscador de usuarios
	 */
	app.get("/finder/:userName", function(req, res) {

			var params = {};
			params.userName = req.params.userName;
			
			finderController.findUsersByPartialName(params, function(err, users) {
				if (err) return res.json({ error: 101,
                                           errorMessage: "Error finding users"});
				res.send(users);
			});

	});

}
