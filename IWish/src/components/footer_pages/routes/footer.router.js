const footerController = require("../controller/footer.controller.js");
const path  = require('path');

var message = "Your suggestion has been sent";

module.exports = function(app) {

	/**
	 * Router de pagina de privacidad
	 */
	app.get('/privacity', function(req, res){
		res.render('../views/footer_pages/privacity.pug');
	});
	/**
	 * Router de pagina de legalidad 
	 */
	app.get('/legal', function(req, res){
		res.render('../views/footer_pages/legal.pug');
	});
	/**
	 * Router de pagina de contacta con nosotros
	 */
	app.get('/contact_us', function(req, res){
		res.render('../views/footer_pages/contact_us.pug');
	});

	/**
	 * Formulario contacta con nosotros, 
	 * gestiona los datos y envia un email al correo de la aplicación
	 * con el texto introducido en el formulario. 
	 */
	app.post('/contact', function(req, res){
		
		var params = {};
    	params.email = req.body.email;
    	params.name = req.body.name;
    	params.subject = req.body.subject;
    	params.comment = req.body.comment;

		footerController.sendEmail(params, function(err){

			if (err) return res.render(PATH_ERROR);
			res.render('../views/footer_pages/contact_us.pug', {message: message});

		});

	})

}