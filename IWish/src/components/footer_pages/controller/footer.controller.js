/* Modulos */
const nodemailer = require("nodemailer");
const path       = require('path');


var smtpTransport = nodemailer.createTransport("SMTP", {
        service: "Gmail",
        auth: {
            user: "noreply.iwish@gmail.com",
            pass: "1W15h!B3niS*"
        }
    });

module.exports = {
	/**
	 * Metodo de enviar un email con los datos pasados por params.
	 */
	sendEmail : function(params, callback) {
		var mailOptions = {
			to : "noreply.iwish@gmail.com",
			subject : params.subject,
			html : "<p> From "+ params.email +"</p><p>My name is "+ params.name +" </p><p>"+params.comment
		}
		smtpTransport.sendMail(mailOptions, function(error, response){
			if (error) {
				//@FIXME
				return callback(error, response);
			} 
			return callback(null, response);
		});
	}

}
