const path = require("path");
const PATH_ERROR = path.resolve('../views/error/error.pug');

var message = {
    err         : "There has been some error, try again.",
    send        : "Success! Check your email to recover your password.",
    signup      : "Email is been sent at Please check inbox",
    singup_err  : "That email is already in use, in facebook or local.",
    login_err   : "Invalid email or password."
}

module.exports = function(app, passport) {

	/*
	* Login by facebook
	*/
	app.get('/auth/facebook', passport.authenticate('facebook', { scope: ["email", "user_birthday"] } ));
	app.get('/auth/facebook/callback',
		passport.authenticate('facebook', {
			failureRedirect: '/failure/facebook' }),
			function(req, res) {
			// Successful authentication, redirect home.
			res.redirect('/profile');
		});	

    app.get('/failure/facebook', function(req, res){
        if (!req.isAuthenticated()) {
            res.render('../views/login/login.pug', {message: message.singup_err});
        } 
    })

	// connect = authorize facebook (link accounts)
    app.get('/connect/facebook', passport.authorize('facebook', { scope : ["email", "user_birthday"] }));

    // handle the callback after facebook has authorized the user
    app.get('/connect/facebook/callback',
        passport.authorize('facebook', {
            successRedirect : '/profile',
            failureRedirect : '/'
        }));

    //unlink
	app.get('/unlink/facebook', function(req, res) {

		var user            = req.user;

		//check if has no more links to account
		if (typeof user.google.token == "undefined" && !user.local.verified) return res.render(PATH_ERROR, { errorMessage: "Cannot unlink all accounts."});

		user.facebook.token = undefined;
		user.save(function(err) {
			res.redirect('/profile');
		});
	});
}