const path 	     = require('path');
const PATH_LOGIN = path.resolve('../views/login/login.pug');

module.exports = function(app) {
	/**
 	 * Desconectarse de la aplicacion
 	 */
	var logout = function(req, res) {
		req.logout();
		res.redirect('/login');
	}

	/**
 	 * Pagina de login. 
 	 * Comprueva que no estes logeado, en el caso de estarlo no vas a la pagina de login
 	 */
	var login = function(req, res){
		if (req.isAuthenticated()) {
			res.redirect('/');
		} else {
			res.render(PATH_LOGIN);
		}
	};
	
	app.get("/login", login);

    app.get("/logout", logout);
}