const confirmEmail = require('../controller/login.local.confirm.controller.js');

const path       = require('path');

const PATH_ERROR = path.resolve('../views/error/error.pug');

var message = {
	err  		: "There has been some error, try again.",
	send 		: "Success! Check your email to recover your password.",
	signup 		: "Email is been sent at Please check inbox",
	singup_err	: "That email is already in use.",
	login_err	: "Invalid email or password."
}

module.exports = function(app, passport) {

	/*
	* Login local
	*/	
	app.post('/login', 	passport.authenticate('local-login', {
		successRedirect : '/profile',
		failureRedirect : '/loginError'
	}));

	// process the signup form
	app.post('/signup', passport.authenticate('local-signup', { 
		successRedirect : '/signup-c', 
		failureRedirect: '/failureRedirect' 
	}));

	/**
 	 * Sign up realizado, hacemos logout para que no entres sin haber confirmado el email
 	 */
	app.get('/signup-c', function(req, res){
		req.logout();
		res.render('../views/login/login.pug', {message: message.signup});
	});

	/**
 	 * Router error de sign up
 	 */
	app.get('/failureRedirect', function(req, res){
		if (!req.isAuthenticated()) {
			res.render('../views/login/login.pug', {message: message.singup_err});
		} 
	})

	/**
 	 * Router error en el login
 	 */
	app.get('/loginError', function(req, res){
		if (!req.isAuthenticated()) {
			res.render('../views/login/login.pug', {message: message.login_err});
		} 
	})

	//Connect local
	
	app.get('/connect/local', function(req, res) {
			res.redirect('/login');
		});
	app.post('/connect/local', passport.authenticate('local-signup', {
		successRedirect : '/connectLocalSuccess', // redirect to the secure profile section
		failureRedirect : '/connectLocalError', // redirect back to the signup page if there is an error
	}));
	
	app.get("/connectLocalError", function(req, res) {
		//res.render('../views/profile/profile.pug', {error : 3});
		res.redirect("/profile"); //esto se hará con ajax para pasar el error.
	});

	app.get("/connectLocalSuccess", function(req, res) {
		//res.render('../views/profile/profile.pug', {error : 0});
		res.redirect("/profile"); //esto se hará con ajax para pasar el error.
	});

	// Unlink
	app.get('/unlink/local', function(req, res) {

		var user            = req.user;

		//check if has no other links to account
		if (typeof user.facebook.token == "undefined" && typeof user.google.token == "undefined") return res.render(PATH_ERROR, { errorMessage: "Cannot unlink all accounts."});
		user.local.email    = undefined;
		user.local.password = undefined;
		user.local.verified = false;
		user.save(function(err) {
			if (err) return res.render(PATH_ERROR);
			res.redirect('/profile');
		});
	});	


	/**
 	 * Router de verificación de email.
 	 */
    app.get('/verify/:id',function(req,res){
    	var params = {};
    	params.token = req.params.id;
    	confirmEmail.verify(params, function(err, data){
    		if(data.status) {
    			res.redirect('/login');
    		} else {
    			res.render('', {error: err.message});
    		}
    	})
    });


    //@FIXME enviar correo
    app.post('/recover', function(req, res){
    	var params = {};
    	params.email = req.body.email;
    	confirmEmail.forgotPassword(params, function(err, data){
    		if(err){
    			res.render('../views/login/login.pug', {message: message.err})
    		} else {
    			res.render('../views/login/login.pug', {message: message.send});
    		}

    	})
    });

    //@FIXME RECUPERAR PASSWORD 
    /**
 	 * Router que te envia a la pagina de recuperación de email
 	 */
    app.get('/recover/:id', function(req, res){
    	var params = {};
    	params.token = req.params.id;
    	confirmEmail.verify(params, function(err, data){
    		if(data.status) {
    			res.render('../views/recover_pass/recover_pass.pug');
    		} else {
    			res.redirect('/login');
    		}
    	})
    	// crear pagina de recuperación de password si el verify es correcto te enviará a esa pagina
    })
    /**
 	 * Envia los datos del formulario de recuperación de password
 	 */
    app.post('/recover/:id', function(req, res){
    	var params = {};
    	params.token = req.params.id;
    	params.password = req.body.password;
    	params.confirmPassword = req.body.password2;
    	// funcion que recupera el password enviado en la pagina de recuperar passwords y lo guarda
    	confirmEmail.changePassword(params, function(err, data){
			if(data.status) {
    			res.redirect('/login');
    		} else {
    			res.redirect('/login');
    		}
    	})
    });
}
