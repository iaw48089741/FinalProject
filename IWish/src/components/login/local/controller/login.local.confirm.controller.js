/* Modulos */
const nodemailer = require("nodemailer");
const path       = require('path');
/* Path */
const PATH_LOGIN = path.resolve('../views/login/login.jade');
const PATH_USER  = '../../../../model/user.js';
const PATH_VERIFY_EMAIL = '../../../../model/verifyEmail.js';
/* bbdd */
const User       = require(PATH_USER);
const VerifyEmail= require(PATH_VERIFY_EMAIL);

var smtpTransport = nodemailer.createTransport("SMTP", {
        service: "Gmail",
        auth: {
            user: "noreply.iwish@gmail.com",
            pass: "1W15h!B3niS*"
        }
    });

/**
 * Metodo enviar email de confirmacion
 */
function sendMail(params, callback) {
        var verifyEmail = new VerifyEmail();
        verifyEmail.email = params.email;
        verifyEmail.token = verifyEmail._id;
        verifyEmail.id_user = params.user._id;
        verifyEmail.save(function(err){
            if(err) {
                // @FIXME
                return callback(err, {});
            } else {
                var host = params.host;
                 if(params.wasForgotten) {
                    var link = "http://localhost/recover/" + verifyEmail.token;
                    var mailOptions = {
                        to : params.email,
                        subject : "Please confirm your Email account",
                        html : "<p>To recovery your password <br> Please click"
                        + "<a href=" + link + ">here</a> </p>" 
                    }
                } else {
                    var link = "http://localhost/verify/" + verifyEmail.token;
                    // @FIXME: Crear template con el email
                    var mailOptions = {
                        to : params.email,
                        subject : "Please confirm your Email account",
                        html : "<p>Welcome to IWish. <br> To verify the creation of your account you just have to click on this link and validate it:</p>"
                        + "<a href=" + link + ">Validate your account</a>" 
                    }
                }
               
                smtpTransport.sendMail(mailOptions, function(error, response){
                    if (error) {
                        //@FIXME
                        return callback(error, response);
                    } 
                    return callback(null, response);
                });
            }
        })


    }

module.exports = {

    errors: {
        USER_NOT_FOUND : "User not found",
        PASSWORD_MISMATCH: "The password fields do not match",
        BAD_TOKEN : "Wrong confirmation token",
        BAD_HOST : "Host mismatch"
    },

    sendEmail: sendMail,
    /**
     * Metodo que apartir del correo enviado comprueba que la url sea correcta con la enviada 
     */
    verify: function(params, callback) {
        VerifyEmail.findOne({'token': params.token}, function(err, emailConfirm){
            if(err) {                 
                return callback(new Error(errors.BAD_TOKEN), {status : false});
            }
            User.findOne({ '_id' :  emailConfirm.id_user }, function(err, user) {
                if (err) {
                    //@FIXME
                    return callback(err, {status : false});
                }
                if (!user) {
                    return callback(new Error(errors.USER_NOT_FOUND), {status : false});
                }
                user.local.verified = true;
                user.save(function(err) {
                    if(err) {
                        //@FIXME
                        return callback(err, {status : false});
                    }
                    return callback(null, {status : true, email: emailConfirm.email});
                }); 
            });
        })
    },
    /**
     * Metodo forgot password
     */
    forgotPassword: function(params, callback) {
        User.findOne({'local.email' : params.email}, function(err, user){
            if(err) {
                return callback(err, {status : false});
            }
            if(!user){
                return callback("notExist", {status : false});
            }
            params.wasForgotten = true;
            params.user = user;

            sendMail(params, callback);

        })
    },
    /**
     * Funcion de cambiar la contraseña si te has olvidado
     */
    changePassword: function(params, callback) {
        if (params.password != params.confirmPassword) {
            return callback(new Error(errors.PASSWORD_MISMATCH), { status: false });
        }
        VerifyEmail.findOne({'token': params.token}, function(err, emailConfirm){
            if(err) {                 
                return callback(new Error(errors.BAD_TOKEN), {status : false});
            }
            User.findOne({ '_id' :  emailConfirm.id_user }, function(err, user) {
                if (err) {
                    //@FIXME
                    return callback(err, {status : false});
                }
                if (!user) {
                    return callback(new Error(errors.USER_NOT_FOUND), {status : false});
                }
                user.changePassword(params.password, function(err){
                    if(err) {
                        //@FIXME
                        return callback(err, {status : false});
                    }
                    return callback(null, {status : true, email: emailConfirm.email});
                });
            });
        })
    }



}
