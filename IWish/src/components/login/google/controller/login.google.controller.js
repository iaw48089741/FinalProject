const PATH_USER = '../../../../model/user.js';

const https = require("https");
const GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;

const User              = require(PATH_USER);
const configAuth        = require('../../auth');
const Post  = require("../../../../model/post.js");
const Image = require("../../../../model/image.js");

module.exports = function(passport) {
    passport.use(new GoogleStrategy({

        clientID        : configAuth.googleAuth.clientID,
        clientSecret    : configAuth.googleAuth.clientSecret,
        callbackURL     : configAuth.googleAuth.callbackURL,
        returnURL       : null,
        passReqToCallback : true // allows us to pass in the req from our route (lets us check if a user is logged in or not)

    },
    function(req, token, refreshToken, profile, done) {
        // make the code asynchronous
        // User.findOne won't fire until we have all our data back from Google
        process.nextTick(function() {
            if (!req.user) {
                // try to find the user based on their google id
                User.findOne({ '$or' : [{ 'google.email' : profile.emails[0].value}, {'facebook.email' : profile.emails[0].value  }, {'local.email' : profile.emails[0].value  }]}, function(err, user) {
                    if (err)
                        return done(err);
                    if (user) {
                        if(user.facebook.email == profile.emails[0].value) {
                            return done(null, false);
                        }
                        // if there is a user id already but no token (user was linked at one point and then removed)
                    	if (user.google.id && !user.google.token) {
                            user.google.token = token;
                            user.google.name  = profile.displayName;
                            user.google.email = profile.emails[0].value;

                            user.save(function(err) {
                                if (err)
                                    throw err;

                                // if successful, return the new user
                                return done(null, user);
                            });
                        } else if(user.google.id == profile.id) {
                            //regular login
                            return done(null, user);
                        } else if(user.email == profile.emails[0].value) {
                            console.log("dentro");
                            return done(null, false);
                        }
                        return done(null, false);
                    } else {
                        // if the user isnt in our database, create a new user

                        var newUser          = new User();

                        // set all of the relevant information
                        newUser.email        = profile.emails[0].value
                        newUser.google.id    = profile.id;
                        newUser.google.token = token;
                        newUser.google.name  = profile.displayName;
                        newUser.google.email = profile.emails[0].value; // pull the first email

                        //newUser.emails.push.apply(newUser.emails, profile.emails);

                        //crear primer post q sera el perfil (nivel 0)
                        var new_post = new Post();
                        new_post.level_post = 0;
                        new_post.post_type = "profile";
                        new_post.post_date = new Date();
                        new_post.edit_date = new Date();
                        new_post.id_user = newUser._id;

                        //@FIXME IMAGEN GOOGLE
                        /*var new_image = new Image();
                        var reqOptions = genProfilePicReq(profile.id);

                        https.request(reqOptions, function(res) {
                            // FIXME: check status code
                            body = JSON.parse(body);
                            if (err) done(err);
                            //si es default, ponemos nuestra default
                            if (body.image.isDefault) {
                                new_image.value = "../user_images/foto_basica.jpg";
                            } else {
                                new_image.value = body.image.url;
                            }

                            new_post.images.push(new_image);

                            newUser.profile = new_post;

                            //newUser.emails.push.apply(newUser.emails, profile.emails);
        					newUser.name = profile.name.givenName;
                            newUser.username = profile.name.givenName;
                            newUser.lastname = profile.name.familyName;
                            //newUser.local.email = profile.emails[0].value;
                            if (newUser.birth_date) newUser.birth_date = new Date(profile._json.birthday);
                             
                            // save the user
                            newUser.save(function(err) {
                                if (err)
                                    throw err;
                                return done(null, newUser);
                            });
                            
                        })*/

                        //quitar esto cuando se haya hecho el fixme
                        var new_image = new Image();
                        new_image.value = "../user_images/foto_basica.jpg";
                        new_post.images.push(new_image);
                        newUser.profile = new_post;
                        //newUser.emails.push.apply(newUser.emails, profile.emails);
                        newUser.name = profile.name.givenName;
                        newUser.username = profile.name.givenName;
                        newUser.lastname = profile.name.familyName;
                        //newUser.local.email = profile.emails[0].value;
                        if (newUser.birth_date) newUser.birth_date = new Date(profile._json.birthday);
                        newUser.save(function(err) {
                                if (err)
                                    throw err;
                                return done(null, newUser);
                            });

                    }
                });
            } else {
                 // user already exists and is logged in, we have to link accounts
                var user = req.user; // pull the user out of the session

                /*if (!user.google.id) {
                    user.emails.push.apply(user.emails, profile.emails);
                }*/
                user.google.id    = profile.id;
                user.google.token = token;
                user.google.name  = profile.displayName;
                user.google.email = profile.emails[0].value; // pull the first email
                user.save(function(err) {
                    if (err)
                        throw err;
                    return done(null, user);
                });
            }
        });

    }));

};

function genProfilePicReq(profileID) {

    return { hostname: "www.googleapis.com", 
             path: "/plus/v1/people/" + profileID + "?fields=image&key=AIzaSyBYGCeP7zmUSFZDg-TeTM4NuTSe1C14-rQ",
             method: "GET",
             port: 443,
              };

}