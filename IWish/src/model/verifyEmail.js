const mongoose = require('mongoose');

// objecte esuqema

var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

// instància de l'esquema
var verifyEmailSchema = new Schema({
	token : String,
	id_user : ObjectId,
	email: String,
	creation_date: { type: Date, default: Date.now }
});


// registrem la col·lecció Users amb un esquema concret i exportem
module.exports = mongoose.model('VerifyEmail', verifyEmailSchema);