const express    = require("express");
const bodyParser = require("body-parser");
const apiAuth    = require("../components/api_commons/middleware/api_commons.auth.js");

const appi       = express.Router();


appi.use(bodyParser.json());
appi.use(apiAuth);

//*** post ***//
require("../components/post/routes/post.api.router.js")(appi); 

//*** finder ***//
require("../components/finder/routes/finder.api.router.js")(appi);

//*** profile ***//
require("../components/profile/routes/profile.api.router.js")(appi);

module.exports = appi;
